Almost all my work, open and closed, is in teams.

Open source teams:

- `Zope Foundation <https://github.com/zopefoundation>`_

- `Zope Corporation <https://github.com/zc>`_

- `Buildout <https://github.com/buildout>`_
